package restcontroller;

import feign.Client;
import feign.okhttp.OkHttpClient;
import okhttp3.JavaNetCookieJar;
import org.junit.Rule;
import org.junit.rules.ErrorCollector;
import ru.ovechkin.tm.entity.Project;
import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.rest.controller.IAuthenticationRestController;

import java.net.CookieManager;
import java.net.CookiePolicy;

import static ru.ovechkin.tm.rest.controller.IProjectRestController.client;

public class IProjectRestControllerTest {

    final CookieManager cookieManager = new CookieManager();
    {
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
    }

    final okhttp3.OkHttpClient.Builder builder =
            new okhttp3.OkHttpClient().newBuilder();
    {
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
    }

    final Client client = new OkHttpClient(builder.build());

    final IAuthenticationRestController auth = IAuthenticationRestController.client(client);

    {
        auth.login("user", "user");
    }

    @Rule
    public final ErrorCollector errorCollector = new ErrorCollector();

    @Test
    public void findAllTest() {
        System.out.println(client(client).allProjects());
        Assert.assertTrue(client(client).allProjects().size() > 0);
    }

    @Test
    public void createTest() {
        final Project project = new Project();
        project.setName("NameFromFeignClient1client");
        project.setDescription("DescriptionFromFeignClient1client");
        client(client).create(project);
        try {
            Assert.assertTrue(client(client).allProjects().contains(project));
        } catch (AssertionError error) {
            errorCollector.addError(error);
        }
        client(client).remove(project.getId());
    }

    @Test
    public void removeTest() {
        final Project project = new Project();
        project.setName("ForRemoveFromFeign");
        project.setDescription("ForRemoveFromFeign");
        client(client).create(project);
        client(client).remove(project.getId());
        Assert.assertFalse(client(client).allProjects().contains(project));
    }

    @Test
    public void updateTest() {
        final Project project = new Project();
        project.setName("ForUpdateFromFeign");
        project.setDescription("ForUpdateFromFeign");
        client(client).create(project);
        final Project projectUpdated = new Project();
        projectUpdated.setName("UpdatedFromFeign");
        projectUpdated.setDescription("UpdatedFromFeign");
        client(client).edit(project.getId(), projectUpdated);
        try {
            Assert.assertTrue(client(client).allProjects().contains(projectUpdated));
        } catch (AssertionError error) {
            errorCollector.addError(error);
        }
        client(client).remove(project.getId());
    }
}