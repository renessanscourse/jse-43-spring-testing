package ru.ovechkin.tm.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.config.WebMvcConfig;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumerated.Role;
import ru.ovechkin.tm.repository.UserRepository;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebMvcConfig.class)
public class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Lazy
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IUserService userService;

    @Test
    public void testFindByLogin() {
        final User user = new User();
        user.setId("constantId");
        user.setLogin("test");
        user.setPasswordHash(passwordEncoder.encode("test"));
        user.setRole(Role.USER);
        userRepository.save(user);
        Assert.assertEquals(user.toString(), userService.findByLogin(user.getLogin()).toString());
    }

    @Test
    public void testRegistry() {
        final User user = new User();
        user.setId("constantId");
        user.setLogin("test");
        user.setPasswordHash(passwordEncoder.encode("test"));
        Assert.assertNull(userService.findByLogin(user.getLogin()));
        userService.registry(user);
        Assert.assertNotNull(userService.findByLogin(user.getLogin()));
    }

}