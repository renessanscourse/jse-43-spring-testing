package ru.ovechkin.tm.exeption.other;

public class NameAlreadyTakenException extends RuntimeException {

    public NameAlreadyTakenException() {
        super("Error! This name is already taken...");
    }

    public NameAlreadyTakenException(final String name) {
        super("Error! This name [" + name  + "] is already taken...");
    }


}