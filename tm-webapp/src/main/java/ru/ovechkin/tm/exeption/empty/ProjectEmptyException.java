package ru.ovechkin.tm.exeption.empty;

public class ProjectEmptyException extends RuntimeException {

    public ProjectEmptyException() {
        super("Error! Project is empty...");
    }

}