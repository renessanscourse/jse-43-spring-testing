package ru.ovechkin.tm.exeption.user;

public class SelfRemovingException extends RuntimeException {

    public SelfRemovingException() {
        super("Error! You can't remove yourself...");
    }

}
