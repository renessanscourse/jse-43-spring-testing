package ru.ovechkin.tm.exeption.empty;

public class CommandEmptyException extends RuntimeException {

    public CommandEmptyException() {
        super("Error! Command is empty...");
    }

}